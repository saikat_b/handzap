$(document).ready(function(){
	$('#title1').on('click',function(){
		$('#title1').addClass('active');
		$('#title2').removeClass('active');
		var text = $('#title1').text();
		$('#contentTitle').text(text);
	});
	$('#title2').on('click',function(){
		$('#title2').addClass('active');
		$('#title1').removeClass('active');
		var text = $('#title2').text();
		$('#contentTitle').text(text);
	});

	$('#tab1').on('click',function(){
		$('#tab1').addClass('active');
		$('#tabActive').css('display','block');
		$('#tab2').removeClass('active');
		$('#tabDeleted').css('display','none');
	});
	$('#tab2').on('click',function(){
		$('#tab2').addClass('active');
		$('#tabDeleted').css('display','block');
		$('#tab1').removeClass('active');
		$('#tabActive').css('display','none');
	});
})